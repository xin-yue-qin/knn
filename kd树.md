**前情回顾：**

> 假如我们现在有一个数据集，当给定一个新的实例时，最简单粗暴的方法就是计算它和所有点的距离，然后找到k个最近邻，最后根据多数表决的规则判断这个实例所属的类。

但是如果这个数据集中的训练实例非常多且密集，而且一个实例就有很多特征，那么就要计算出成千上万个距离，运算量极其庞大。

这时，我们就可以用一个更快速的计算方法——kd树。

# 什么是kd树

kd 树从根本上看，是一个二叉树结构，根据这个结构对k维空间进行不断的划分，每一个结点就代表了k维超矩形区域。

二维（k=2）的矩形区域：

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/two-demensional-kd_tree.png" alt="二维矩形区域" style="zoom:50%;" />

三维（k=3）的矩形区域：

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/3-demesional_kd_tree.png" alt="三维矩形区域" style="zoom:50%;" />

> 注意，kd树这里的k代表的是特征的个数，也就是数据的维度，而k近邻中的k指的是距离新实例点最近的k个邻居。

# 如何构造kd树

## 原理

**输入：** k维空间数据集：
$$
T=\lbrace x_1,x_2,...,x_n \rbrace
$$
x<sub>i</sub>=(x<sub>i</sub><sup>(1)</sup>，x<sub>i</sub><sup>(2)</sup>，...，x<sub>i</sub><sup>(k)</sup>)<sup>T</sup>，x<sub>i</sub><sup>(l)</sup>的上标代表的是第l个特征值

**输出：** kd树

### 1.开始：构造根结点。

在根结点处选择一个最优特征进行切割，一般通过比较每个特征上的方差来决定，方差最大的特征就是我们要选取的**坐标轴**。

假如我们选出来的是x<sup>(1)</sup>，以它作为坐标轴，找切分点，将超矩形区域切割为两个子区域。通常选取x<sup>(1)</sup>方向上数据的中位数作为切分点，由根结点生出深度为 1 的左右子结点，左结点的坐标小于切分点，右结点坐标大于切分点。

### 2.重复：剩余特征的选取与切割

继续对深度为j的结点，选择x<sup>(1)</sup>为切分坐标轴，l=j(mod k)+1，以该结点区域中所有实例x<sup>(1)</sup>坐标的中位数作为切分点，将区域不断分割为两个子区域。

### 3.停止：得到kd树

直到两个子区域没有实例时停止分割，即得到一棵 kd 树。

## 例题解说

**输入：**
$$
T=\lbrace(2,3),(5,4),(9,6),(4,7),(8,1),(7,2)\rbrace
$$
**输出：** kd 树

为了方便，给数据集里的数据点标号后进行可视化展示：

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/kd_tree_input.png" style="zoom:50%;" />

### 1.第一次切分

因为该训练数据集的维度是2，那么任选一个特征即可。不妨选择x<sup>(1)</sup>为坐标轴，将x<sup>(1)</sup>中的数据按照从小到大排序，分别是：2，4，5，7，8，9

中位数不妨选7，即**以（7，2）为根结点**，切分整个区域。

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/first_cut.png" style="zoom:50%;" />

左边的就是小于7的子结点，右边的是大于7的子结点。

### 2.第二次切分

再次划分区域：

对第一个特征加1，以x<sup>(2)</sup>为坐标轴。

对于第一次切分后的左边区域而言，将x<sup>(2)</sup>中的数据按照从小到大排序，分别是：2，3，4，7

中位数取4，切分点坐标为**（5，4）**，再画一条横线进行第二次切分。

同样地，对第一次切分后的右边区域而言，将x<sup>(2)</sup>中的数据按照从小到大排序，分别是：1，6

因为只有两个点，不妨选择6作为切分点，右边区域切分点坐标为**（9，6）**，画一条横线进行第二次切分。

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/second_cut.png" style="zoom:50%;" />

### 3.继续切分

第二个特征加1，以x<sup>(3)</sup>为坐标轴，而这里只有x<sup>(1)</sup>和x<sup>(2)</sup>，所以只对x<sup>(1)</sup>进行划分。或者直接计算2(mod 2)+1=1即可得到所选的特征。

可以明显看出**A（2，3）**，**D（4，7）**，**E（8，1）**三个实例点还未切分，那么以这些点为根结点分别画一条垂直于x<sup>(1)</sup>轴的线进行切分。

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/third_cut.png" style="zoom:50%;" />

### 4.绘制kd树

这几次划分中的根结点分别是：

第一级：（7，2）

第二级：（5，4），（9，6）

第三级：（2，3），（4，7），（8，1）

根据这个层次绘制出kd树：

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/final_kd_tree.png" style="zoom:50%;" />

# 如何搜索kd树

## 原理

**输入：** 已构造的kd树，目标点是x

**输出：** x的最近邻

**寻找“当前最近点”：** 从根结点出发，递归访问kd树，找出包含x的叶结点，以此叶结点为“当前最近点”

**回溯：** 以目标点和”当前最近点“的距离沿树根部进行回溯和迭代，当前最近点一定存在于该结点一个子结点对应的区域，检查子结点的父结点的另一子结点对应的区域是否有更近的点。

当回退到根结点时，搜索结束，最后的”当前最近点“即为x的最近邻点。

## 例题解说

以上文生成的那棵kd树为例：

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/kd_tree_search_input.png" style="zoom:50%;" />

### Case 1

**输入：** kd树，目标点 x=(2.1,3.1)

**输出：** 该目标的最近邻点

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/kd_tree_search_case1.jpg" style="zoom:50%;" />

**寻找当前最近点：** 从根结点开始，x=(2.1,3.1)在根结点（7，2）的左子区域内，继续到（5，4）所确定的左子区域内，继续到（2，3）的右子区域中，（2，3）就是当前最近邻点。

**回溯：** 以（2.1，3.1）为圆心，以其与已确定的最近邻点之间的距离为半径画一个圆，这个区域里没有其他的点，那就证明（2，3）是（2.1，3.1）的最近邻点。

### Case 2

**输入：** kd树，目标点 x=（2，4.5）

**输出：** 该目标点的最近邻点

<img src="https://gitee.com/xin-yue-qin/knn/raw/master/img/kd_tree_search_case2.jpg" alt="kd_tree_search_case2" style="zoom:50%;" />

**寻找当前最近点：** 从根结点开始，x=（2，4.5）在根结点（7，2）的左子区域内，继续到（5，4）所确定的上子区域内，继续到（4，7）的左子区域中，（4，7）就是当前最近邻点。

**回溯：** 我们以（2，4.5）为圆心，以（2，4.5）到（4，7）两点之间的距离为半径画一个圆，这个区域内有两个结点，分别是（2，3）和（5，4），通过计算（2，4.5）到这两点的距离，得出到（2，3）距离最近，那么（2，3）就是最邻近点。接着再以（2，4.5）为圆心，以（2，4.5）到（2，3）两点之间的距离为半径画一个圆，此时圆里没有其他的结点，说明可以确认（2，3）就是（2，4.5）的最近邻点。



#### 参考资料：

[简博士数据分析吧](https://mp.weixin.qq.com/s?__biz=MzI0MjAwOTg4MQ==&mid=2247486223&idx=1&sn=63a59f2a8261fd597eda62f78d55a812&scene=21#wechat_redirect)

#### 视频资料：

[3.3 k近邻法-什么是kd树](https://mp.weixin.qq.com/cgi-bin/appmsg?t=media/appmsg_edit_v2&action=edit&isNew=1&type=77&createType=0&token=510735986&lang=zh_CN)

[3.3 k近邻法-构造kd树](https://mp.weixin.qq.com/cgi-bin/appmsg?t=media/appmsg_edit_v2&action=edit&isNew=1&type=77&createType=0&token=510735986&lang=zh_CN)

[3.3 k近邻法-搜索kd树](https://mp.weixin.qq.com/cgi-bin/appmsg?t=media/appmsg_edit_v2&action=edit&isNew=1&type=77&createType=0&token=510735986&lang=zh_CN)